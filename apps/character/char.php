<?php

// Uncomment to enable debug
//ini_set("display_errors", '1');

// Include API from https://github.com/viion/XIVPads-LodestoneAPI
include("../../api/api.php");
	// Initialize a LodestoneAPI Obkect
$API = new LodestoneAPI();

// Retrieve character name and server name from form.  Can also set as string
$characterName = $_POST["name"];
$server = $_POST["server"];

// Get character info using API
$Character = $API->get(array(
  "name" => $characterName, 
  "server" => $server
));


// Print character portrait
echo '<img src="'. $Character->getPortrait() .'" />';


// Get the character's 
$FC = $Character->getFreeCompany();


// Print character info
echo '<br /><b>Character Name: </b>' . $characterName . '<br />';
echo '<b>Free Company: </b>' . $FC['name'] . '<br />';
echo '<b>Free Company ID: </b>' . $FC['id'] .'<br />';
echo '<b>Free Company Members: </b><br />';

// Get FC Array
$FCSearchResult = $API-> searchFreeCompanyMembersById($FC['id']);
$FCGetResult = $API->getSearchFreeCompanyMembers();

// Display FC members
foreach($FCGetResult['results'] as $resultSet) {

// Uncomment to enable display of portraits of all characters in FC (Takes a LONG time)
//	echo '<img src="'. $FCChar->getPortrait() .'" /><br />';

	echo $resultSet[0] . "<br />";
}

?>
